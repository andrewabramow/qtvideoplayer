import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("QML_Player")

    Image {
        id:image
         source: "big_floppa_default.png"
         anchors.horizontalCenter: parent.horizontalCenter
         anchors.top: parent.top
         anchors.topMargin: 30
    }

    Button {
            text: "▶️"
            font.pointSize: 24
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 30
            anchors.horizontalCenterOffset: -200
            onClicked: {
                        image.source = "big_floppa_play.png";
                        progress_bar.value = 0.5;
                    }

        }
    Button {
            text: "⏸️"
            font.pointSize: 24
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 30
            anchors.horizontalCenterOffset: -100
            onClicked: {
                        image.source = "big_floppa_pause.png";
                    }
        }
    Button {
            text: "⏹"
            font.pointSize: 24
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 30
            onClicked: {
                        image.source = "big_floppa_stop.png";
                    }
        }
    Button {
            text: "⏪️"
            font.pointSize: 24
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.horizontalCenterOffset: 100
            anchors.bottomMargin: 30
            onClicked: {
                        image.source = "big_floppa_rewind.png";
                    }
        }
    Button {
            text: "⏩️"
            font.pointSize: 24
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.horizontalCenterOffset: 200
            anchors.bottomMargin: 30
            onClicked: {
                        image.source = "big_floppa_forward.png";
                    }
        }
    ProgressBar {
        id: progress_bar
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        value: 0.1

        }
}

